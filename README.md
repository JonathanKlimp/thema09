# README #

### General ####

In order to predict wheter or not an animal should be quarantined after a biting incident a machine learning 
algorithm needs to be made. This repo contains the research process of finding this algorithm.

Version 1.0.0

### Layout ###

* The data folder contains the datasets that were used and generated within the EDA.
* The EDA folder contains the EDA rmd file and the pdf version.
* The Machine_learning_figs contains figures of the results of the different classifiers.
* The Report folder contains the final report rmd and pdf.

### Setup EDA ###

The EDA was written in [Rstudio](https://rstudio.com/) 1.2.1335 running R version 3.6.0.
Data that has been used in the EDA was collected from [kaggle](https://www.kaggle.com/rtatman/animal-bites).
The goal of the EDA was to clean and investigate the data so that it can be used for machine learning. 
The EDA also contains the steps of finding the optimal machine learning algorithm.
After the data was cleaned and the model was created, the model was saved and integrated in a java application.
This java application can be found in the corresponding [repository](https://bitbucket.org/JonathanKlimp/javawrapper/src/master/)

### Contact ###

For questions please contact me at,

j.klimp@st.hanze.nl